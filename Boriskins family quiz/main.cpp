#include <iostream>
#include <string.h>

using namespace std;


int Choise(const string& initialText, int trueValue ) {
    cout << initialText;
    cout << "1 - sasha" << "\n";
    cout << "2 - mama" << "\n";
    cout << "3 - papa" << "\n";
    cout << "4 - andrey" << "\n";
    cout << "5 - kristina" << "\n";
    cout << "6 - slava" << "\n";
    cout << "Enter the guess: ";
    
    int dogatka;
    cin >> dogatka;
    
    if(dogatka == 9)
        exit(1);
    
    if (dogatka == trueValue) {
        cout << "True\n\n";
    }
    else {
        cout << "False\n\n";
    }
    
    
    return (dogatka == trueValue) ? 1 : 0;
}

int main() {
    cout << "Hello, today I invite you to go through my quiz for knowing the Boriskins family!" << "\n";
    cout << "If you want to quit, press 9.\n";
    
    int correctAnswers = 0;
    
    correctAnswers += Choise("First question.\nWho is velik?\n", 1);
    
    correctAnswers += Choise("Second question.\n Who is the skinny woman?\n",2);
    
    correctAnswers += Choise("Third question.\nWho is the sumo wrestler?\n",5);
    
    correctAnswers += Choise("Fourth question.\nWho is most strong in boriskins family?\n",3);
    
    correctAnswers += Choise("Fifth question.\nWho is Dunichka Sladunechka?\n",4);
    
    double percents = correctAnswers/5.0 * 100;
    
    if(percents > 50)
      cout << "You";
    else
      cout << "";
    
    cout << "That's all!" << "\n";
    cout << "Enter the exit, if you want to exit the program!";
    
    char letter; 
    cin >> letter;
    return 0; 
}
